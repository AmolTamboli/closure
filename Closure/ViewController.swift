//
//  ViewController.swift
//  Closure
//
//  Created by Amol Tamboli on 27/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    typealias completionHandler = ([String:Any]) -> Void
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var completion:completionHandler?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if txtName.text!.isEmpty{
            self.showAlert(title: "Enter Name", msg: "")
        } else if txtPassword.text!.isEmpty{
            self.showAlert(title: "Enter Password", msg: "")
        } else {
            guard let name = txtName.text else {return}
            guard let password = txtPassword.text else {return}
            let dict = ["name":name, "password":password]
            completion!(dict)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension UIViewController {
    func showAlert(title: String, msg: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
