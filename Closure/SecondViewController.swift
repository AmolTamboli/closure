//
//  SecondViewController.swift
//  Closure
//
//  Created by Amol Tamboli on 27/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnHome(_ sender: UIButton) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.completion = {dict in
                self.lblName.text = dict["name"] as! String
                self.lblPassword.text = dict["password"] as! String
            }
            self.navigationController?.pushViewController(vc, animated: true)
    }
}



